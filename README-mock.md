# 自动生成service测试用mock

>  在编写服务的时候，需要对服务做一些集成和联通性质的建议测试，但是构造service box比较麻烦，同时干扰因素也比较多，所这里提供了一种简单的框架，可以针对服务，生成调用和被调用的简易客户端服务器用于做测试

## 使用方法

### 脚本调用：

```shell
python repo.py -t cpp --mock idlname:service_name //生成单个服务的mock工程
python repo.py -t cpp --mock idlname //生成idl中所有服务的调用工程
```

### 生成目录

>  因为考虑到用户测试的服务本身是可能需要第三方的库的，所以这里并没有直接去编译服务生成的mock工程，而是在生成目录生成了基本工程，允许用户修改camke之后自行编译

```shell
service-box\src\repo\tmp\idlname\test\service_name
```

在这个目录下有proxy和server两个文件夹, 下面针对两个文件夹的类容进行说明。

#### proxy

> 简易客户端，可以连接你的server-box 或者 server 代替客户端和你做测试

```shell
├─proxy
│  │  CMakeLists.txt	//生成的cmake文件，需要修改的可以自己添加
│  │  mock_logger.h 	//log接口必要的实现
│  │  mock_transport.cpp 
│  │  mock_transport.h //tranport接口实现，用于讲网络线程过来的消息转发给rpc框架
│  │  proxy.cpp //生成代码，main函数
│  │  servicedynamic_caller.cpp
│  │  servicedynamic_caller.h // 生成代码，caller 实现了对于service方法的逐次调用
│  └─thirdparty //第三方依赖
│      ├─cxxopts //命令行解析
│      └─knet //第三方网络
```

理论上来说porxy是不依赖于其他第三方库的，可以直接编译。

#### server

> 简易服务器，用于测试单个服务的method是否正常

```shell
├─server
    │  CMakeLists.txt //生成的cmake文件，需要修改的可以自己添加
    │  mock_logger.h //log接口必要的实现
    │  mock_transport.cpp
    │  mock_transport.h //tranport接口实现，用于讲网络线程过来的消息转发给rpc框架
    │  server.cpp //生成代码，main函数入口，这里dynamic 和 static 服务生成会不同，如果有自己的依赖需要修改cmake
    └─thirdparty
│      ├─cxxopts //命令行解析
│      └─knet //第三方网络
```

server 主要是提供了加载服务的简易框架，如果你的服务是需要**依赖第三方库**那么请自行修改cmake之后在编译

### 编译

- 在编译前确保你要生成的服务，已经执行了repo的编译指令进行过编译了，当然我的脚本也是会检查报错的

- 进入server和proxy执行命令

```shell
cmake -G "Visual Studio 16 2019" -A x64
cmake --build . --config "Debug"
```

### 使用

proxy和server输出目录为`service-box\src\repo\bin\debug\servicename_proxy` 或者`service-box\src\repo\bin\debug\servicename_server`,编译成功之后，使用`service_name_proxy -p port -h host` 就可以进行启动

## TODO

- [ ] 自动编译proxy
- [ ] 对接service-box gui