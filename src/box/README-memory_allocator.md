# kratos::service::MemoryAllocator

service box内存分配器暴露给用户的调用接口。

## 设计要点

1. 通过MemoryAllocator接口只能分配智能指针类型的对象, 主要是为了支持Bundle被卸载时防止服务持有service box内存池内指针导致内存泄漏
2. 通过snapshot和dump方法可以对用户通过MemoryAllocator分配的内存进行快照和统计信息导出，查看内存泄漏和内存变化趋势的详细信息

