# kratos::service::BoxNetwork

Service box网络TCP通信基础类。

## 通信方式

1. 有一个独立的网络线程，用于socket建立、监听、读、写、关闭
2. 网络线程与主线程通过2条无锁队列通信

## 继承类需要实现的事件接口

1. on_listen 监听器建立成功/失败事件回调
2. on_accept 接受新客户端连接建立事件回调
3. on_connect 主动连接对端成功/失败事件回调
4. on_close 管道关闭事件回调
5. on_data 数据接受事件回调

继承类只需实现上述接口即可