#include "udp_socket.hh"
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64)
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")
#endif // _WIN32
#include "udp_os.hh"
#include <array>
#include <cstdlib>

namespace kratos {
namespace network {

UdpSocket::Buffer::Buffer(const char *data, std::size_t size,
                          const SocketAddress &address) {
  buffer_ = kratos::make_unique_pool_ptr<char>(size + 1);
  data_ = buffer_.get();
  size_ = size;
  address_ = address;
  memcpy(buffer_.get(), data, size);
}

UdpSocket::UdpSocket(SocketType socket) : socket_(socket) {
  // 设置为非阻塞
  setNonblocking(socket_);
  tmp_recv_buffer_ =
      kratos::make_unique_pool_ptr<char>(NETWORK_IOLOOP_UDP_PACKET_MAX_SIZE);
}

UdpSocket::~UdpSocket() {
  if (INVALID_SOCKET != socket_) {
    closesocket(socket_);
  }
}

void UdpSocket::sendto(const char *buffer, std::size_t &length,
                       const SocketAddress &address) {
  writeBuffers_.push_back(
      kratos::make_unique_pool_ptr<Buffer>(buffer, length, address));
}

bool UdpSocket::recvfrom(char *buffer, std::size_t &length,
                         SocketAddress &address) {
  if (readBuffers_.empty()) {
    return false;
  }
  auto *front = readBuffers_.front().get();
  memcpy(buffer, front->data_, front->size_);
  length = front->size_;
  address = front->address_;
  readBuffers_.pop_front();
  return true;
}

void UdpSocket::close() {
  if (isSocket(socket_)) {
    closesocket(socket_);
  }
}

UdpSocket::operator bool() { return isSocket(socket_); }

void UdpSocket::update(std::time_t million) {
  // 运行事件选取循环
  bool isRead = false;
  bool isWrite = false;
  auto error = network::select(socket_, million, !writeBuffers_.empty(), isRead,
                               isWrite);
  if (!error) {
    return;
  }
  if (isWrite) {
    for (auto it = writeBuffers_.begin(); it != writeBuffers_.end();) {
      auto *buffer = it->get();
      // 确保一次发送的包大小不超过NETWORK_IOLOOP_UDP_PACKET_MAX_SIZE
      std::size_t length = buffer->size_ > NETWORK_IOLOOP_UDP_PACKET_MAX_SIZE
                               ? NETWORK_IOLOOP_UDP_PACKET_MAX_SIZE
                               : buffer->size_;
      auto success =
          network::sendto(socket_, buffer->data_, length, buffer->address_);
      if (success && length) {
        if (length == buffer->size_) {
          it = writeBuffers_.erase(it);
        } else {
          buffer->size_ -= length;
          buffer->data_ += length;
          break;
        }
      } else {
        break;
      }
    }
  }
  if (isRead) {
    SocketAddress address;
    auto size = NETWORK_IOLOOP_UDP_PACKET_MAX_SIZE;
    for (;;) {
      auto success =
          network::recvfrom(socket_, tmp_recv_buffer_.get(), size, address);
      if (success && size) {
        readBuffers_.push_back(kratos::make_unique_pool_ptr<Buffer>(
            tmp_recv_buffer_.get(), size, address));
      } else {
        break;
      }
    }
  }
}

} // namespace network
} // namespace kratos
