#pragma once

#include "component/component.hh"
#include "util/module_loader/module_loader.hh"
#include <unordered_map>
#include <unordered_set>
#include <stdexcept>

namespace Json {
class Value;
}

namespace kratos {
namespace config {
class BoxConfig;
}
} // namespace kratos

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace component {

/**
 * 版本号实现类
 */
class VersionImpl : public Version {
  std::string ver_str_;   ///< 版本号字符串
  std::int32_t major_{0}; ///< 主版本号
  std::int32_t minor_{0}; ///< 次要版本号
  std::int32_t patch_{0}; ///< patch

public:
  VersionImpl();
  virtual ~VersionImpl();
  virtual auto to_string() -> const std::string & override;
  virtual auto major() const -> std::int32_t override;
  virtual auto minor() const -> std::int32_t override;
  virtual auto patch() const -> std::int32_t override;
  virtual auto operator>(const Version &rht) const -> bool override;
  virtual auto operator<(const Version &rht) const -> bool override;
  virtual auto operator==(const Version &rht) const -> bool override;
  virtual auto operator>=(const Version &rht) const -> bool override;
  virtual auto operator<=(const Version &rht) const -> bool override;
  virtual auto operator!=(const Version &rht) const -> bool override;
  /**
   * 解析字符串，获得版本号.
   *
   * \param ver_str 版本号字符串
   * \param [IN OUT] error 错误描述
   * \return 版本号实例
   */
  static auto from_string(const std::string &ver_str, std::string &error)
      -> VersionUniPtr;
  /**
   * 设置版本号.
   *
   * \param major 主版本号
   * \param minor 次要版本号
   * \param patch patch
   * \return 版本号实例
   */
  static auto from_number(std::int32_t major, std::int32_t minor,
                          std::int32_t patch) -> VersionUniPtr;
};

/**
 * 用户描述实现类
 */
class DescriptorImpl : public Descriptor {
  using ValueMap = std::unordered_map<std::string, std::string>;
  ValueMap value_map_; ///< {key, value}

public:
  DescriptorImpl();
  virtual ~DescriptorImpl();
  virtual auto get_value(const std::string &key) const
      -> const std::string & override;

public:
  /**
   * 添加用户描述.
   *
   * \param key 键
   * \param value 值
   * \return
   */
  auto add_value(const std::string &key, const std::string &value) -> void;
};

/**
 * 组件实现类
 */
class ComponentImpl : public Component {
  VersionUniPtr ver_ptr_;     ///< 版本号实例
  DescriptorUniPtr desc_ptr_; ///< 用户描述实例
  util::ModuleLoader loader_; ///< dll/so加载器
  using ExportFunc = void *;
  using ExportMap = std::unordered_map<std::string, ExportFunc>;
  ExportMap export_map_;  ///< 导出函数地址表
  bool is_system_{false}; ///< 是否是系统组件s

public:
  ComponentImpl();
  virtual ~ComponentImpl();
  virtual auto is_system() -> bool override;
  virtual auto get_version() -> const Version * override;
  virtual auto get_descriptor() -> const Descriptor * override;
  virtual auto get_func(const std::string &name) -> void * override;

public:
  /**
   * 设置标志
   */
  auto set_system(bool flag) -> void;
  /**
   * 加载dll/so.
   *
   * \param path dll/so路径
   * \param error 错误描述
   * \return true成功, false失败
   */
  auto load(const std::string &path, std::string &error) -> bool;
  /**
   * 设置版本号.
   *
   * \param ver 版本号实例
   * \return
   */
  auto set_version(VersionUniPtr &&ver) -> void;
  /**
   * 设置用户描述.
   *
   * \param desc 用户描述实例
   * \return
   */
  auto set_descriptor(DescriptorUniPtr &&desc) -> void;
};

/**
 * 系统组件实现类
 */
class SystemComponentImpl : public ComponentImpl, public SystemComponent {
  void *init_func_{nullptr}; ///< 初始化函数地址, 如果存在则调用
  void *destroy_func_{nullptr}; ///< 销毁函数地址, 如果存在则调用
  void *tick_func_{nullptr}; ///< 主循环函数地址, 如果存在则调用

public:
  SystemComponentImpl();
  virtual ~SystemComponentImpl();
  /**
   * 启动
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto start(service::ServiceBox *box) -> bool override;
  /**
   * 关闭
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto stop(service::ServiceBox *box) -> bool override;
  /**
   * 当需要组件初始化时调用
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto on_init(service::ServiceBox *box) -> bool override;
  /**
   * 当需要销毁组件时调用
   * @param box 容器指针
   * @return true成功, false失败
   */
  virtual auto on_destroy(service::ServiceBox *box) -> bool override;
  /**
   * 每帧调用
   * @param box 容器指针
   * @param ts 当前时间戳(毫秒)
   * @return true成功, false失败
   */
  virtual auto on_tick(service::ServiceBox *box, std::time_t ts)
      -> bool override;
};

/**
 * 组件工厂实现类
 */
class ComponentFactoryImpl : public ComponentFactory {
  std::string root_dir_; ///< 组件根目录
  using CompPathMap = std::unordered_map<std::string, ComponentPtr>;
  CompPathMap comp_path_map_;         ///<  已经加载的.dll/.so路径
  service::ServiceBox *box_{nullptr}; ///< 服务容器

public:
  ComponentFactoryImpl();
  virtual ~ComponentFactoryImpl();
  virtual auto set_root_directory(const std::string &root_directory)
      -> void override;
  virtual auto load(const std::string &comp_name, std::string &error,
                    const std::string &version = "") -> ComponentPtr override;
  virtual auto load_remote(const std::string &comp_name,
                           const std::string &source, std::string &error,
                           const std::string &version = "")
      -> ComponentPtr override;

public:
  /**
   * 启动
   * @param box 容器
   * @param error 错误描述
   */
  auto start(service::ServiceBox *box, std::string &error) -> bool;
  /**
   * 关闭
   */
  auto stop() -> bool;
  /**
   * 主循环
   * @param ts 当前时间戳(毫秒)
   */
  auto update(std::time_t ts) -> void;

private:
  /**
   * 检查并下载依赖组件.
   *
   * \param deps 依赖关系
   * \param [IN OUT] error 错误描述
   * \return true成功, false失败
   */
  auto resolve_deps(const Json::Value &deps, std::string &error) -> bool;
  /**
   * 下载组件, 只下载组件, 不下载组件相关依赖
   *
   * \param comp_name 组件名
   * \param [IN OUT] error 错误描述
   * \param version 需求版本
   * \param source 远程服务
   * \return true成功, false失败
   */
  auto download(const std::string &comp_name, std::string &error,
                const std::string &version, const std::string &source) -> bool;
  /**
   * 获取版本信息.
   *
   * \param comp_name 组件名
   * \param version 需求版本
   * \param [IN OUT] error 错误描述
   * \return 版本号实例
   */
  auto get_version_info(const std::string &comp_name,
                        const std::string &version, std::string &error)
      -> VersionUniPtr;
  /**
   * 获取最高版本号.
   *
   * \param comp_name 组件名
   * \return 最高版本号
   */
  auto get_highest_version(const std::string &comp_name) -> VersionUniPtr;
  /**
   * 获取适当的版本.
   *
   * \param comp_name 组件名
   * \param version 版本描述
   * \param [IN OUT] error 错误信息
   * \return 版本号
   */
  auto get_proper_version(const std::string &comp_name,
                          const std::string &version, std::string &error)
      -> VersionUniPtr;
  /**
   * 获取配置文件内的版本号.
   *
   * \param v 配置节点
   * \param [IN OUT] version 版本
   * \param [IN OUT] error 错误信息
   * \return true成功, false失败
   */
  auto get_version(const Json::Value &v, std::string &version,
                   std::string &error) -> bool;
  /**
   * 获取指定版本号实例.
   *
   * \param comp_name 组件名
   * \param version 版本
   * \param [IN OUT] error 错误信息
   * \return 版本号实例
   */
  auto get_version(const std::string &comp_name, const std::string &version,
                   std::string &error) -> VersionUniPtr;
  /**
   * 是否已经被加载.
   *
   * \param comp_path 组件路径
   * \return true是, false否
   */
  auto is_loaded(const std::string &comp_path) -> bool;
  /**
   * 从配置获取用户描述.
   *
   * \param root 配置节点
   * \param [IN OUT] error 错误信息
   * \return 用户描述
   */
  auto get_descriptor(const Json::Value &root, std::string &error)
      -> DescriptorUniPtr;
  /**
   * 加载所有配置的组件
   *
   * \param config_ptr 配置指针
   * \param [IN OUT] error 错误信息
   * \return true成功, false失败
   */
  auto load_all(config::BoxConfig *config_ptr, std::string &error) -> bool;
};

} // namespace component
} // namespace kratos
