﻿#pragma once

#include "box_config_impl.hh"
#include <string>
#include <unordered_map>

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace kratos {
namespace config {

/*
 * 从Apollo配置中心拉取配置并加载
 */
class BoxConfigApollo : public BoxConfigImpl {
  std::string local_file_path_; ///< 配置文件本地路径

  using KeyValue = std::unordered_map<std::string, std::string>;
  KeyValue key_value_; ///< 命令行配置表

public:
  /**
   * 构造.
   *
   */
  BoxConfigApollo(kratos::service::ServiceBox *box);
  /**
   * 析构.
   *
   */
  virtual ~BoxConfigApollo();
  /**
   * 加载配置.
   * @param config_file_path 配置文件路径
   * @param [OUT] error 错误
   * @retval true 成功
   * @retval false 失败
   */
  virtual auto load(const std::string &config_file_path, std::string &error)
      -> bool override;
  /**
   * 重新加载配置.
   * @param config_file_path 配置文件路径
   * @param [OUT] error 错误
   * @retval true 成功
   * @retval false 失败
   */
  virtual auto reload(const std::string &config_file_path, std::string &error)
      -> bool override;

private:
  /**
   * 下载配置
   * @param config_file_path 配置文件路径
   * @param [OUT] error 错误
   * @retval true 成功
   * @retval false 失败
   */
  auto download(const std::string &config_file_path, std::string &error)
      -> bool;
};

} // namespace config
} // namespace kratos
