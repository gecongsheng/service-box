#include "interface_descriptor_impl.hh"
#include "util/object_pool.hh"
#include "util/string_util.hh"

kratos::service::InterfaceDescriptorImpl::InterfaceDescriptorImpl() {}

inline kratos::service::InterfaceDescriptorImpl::~InterfaceDescriptorImpl() {}

auto kratos::service::InterfaceDescriptorImpl::parse(
    const std::string &description) -> bool {
  if (description.empty()) {
    return true;
  }
  std::string error;
  return util::get_json_root(description, descriptor_, error);
}

auto kratos::service::InterfaceDescriptorImpl::equal(
    const rpc::InterfaceDescriptor &other) const -> bool {
  const auto &impl_ref = dynamic_cast<const InterfaceDescriptorImpl &>(other);
  return (descriptor_ == impl_ref.descriptor_);
}

kratos::service::InterfaceDescriptorFactoryImpl::
    InterfaceDescriptorFactoryImpl() {}

inline kratos::service::InterfaceDescriptorFactoryImpl::
    ~InterfaceDescriptorFactoryImpl() {}

auto kratos::service::InterfaceDescriptorFactoryImpl::create_descriptor()
    -> rpc::InterfaceDescriptorPtr {
  return kratos::make_shared_pool_ptr<InterfaceDescriptorImpl>();
}
