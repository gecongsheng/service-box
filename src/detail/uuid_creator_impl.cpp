#include "uuid_creator_impl.hh"
#include "util/string_util.hh"

#if defined(WIN32) || defined(WIN64)
#include <Rpc.h>
#pragma comment(lib, "rpcrt4.lib")
#else
#include <uuid/uuid.h>
#endif // defined(WIN32) || defined(WIN64)

namespace kratos {
namespace service {

UUIDCreatorImpl::UUIDCreatorImpl() {}

UUIDCreatorImpl::~UUIDCreatorImpl() {}

bool UUIDCreatorImpl::generate(std::string &uuid_str) {
#if defined(WIN32) || defined(WIN64)
  UUID uuid;
  (void)::UuidCreate(&uuid);
  unsigned char* str;
  (void)::UuidToStringA(&uuid, &str);
  std::string s(reinterpret_cast<char*>(str));
  ::RpcStringFreeA(&str);
#else
  // NOTE LINK libuuid
  uuid_t uuid;
  uuid_generate_random(uuid);
  char s[37];
  uuid_unparse(uuid, s);
#endif //  defined(WIN32) || defined(WIN64)
  uuid_str = util::remove(s, "-");
  return true;
}

} // namespace service
} // namespace kratos
