#pragma once

#include "util/bucket_token/bucket_token_defines.hh"
#include "util/bucket_token/token_bucket.hh"
#include <memory>

namespace kratos {
namespace util {

/**
 * 令牌桶实现类.
 */
class TokenBucketImpl : public TokenBucket {
  TokenConsumerPtr consumer_; ///< 消费者
  TokenProducerPtr producer_; ///< 生产者
  std::size_t volume_{0};     ///< 容量

public:
  /**
   * 构造.
   *
   * \param consumer 消费者
   * \param producer 生产者
   */
  TokenBucketImpl(TokenConsumerPtr consumer, TokenProducerPtr producer);
  virtual ~TokenBucketImpl();
  virtual auto update(std::time_t now) -> void override;
  virtual auto get_consumer() -> TokenConsumerPtr override;
  virtual auto set_volume(std::size_t volume) -> void override;
  virtual auto consume() -> bool override;
};
} // namespace util
} // namespace kratos
