#include "log_history.hh"

kratos::lua::LogHistoryImpl::LogHistoryImpl() {}

kratos::lua::LogHistoryImpl::~LogHistoryImpl() {}

auto kratos::lua::LogHistoryImpl::set_level(int level) -> void { level_ = level; }

auto kratos::lua::LogHistoryImpl::get_history(int limit, std::stringstream &ss)
    -> void {
  if (limit > (int)log_history_.size()) {
    limit = (int)log_history_.size();
  }
  auto start = log_history_.size() - std::size_t(limit);
  for (; (start < log_history_.size()) && (limit > 0); start++) {
    limit -= 1;
    ss << log_history_[start] << std::endl;
  }
}

auto kratos::lua::LogHistoryImpl::log(const char *line) -> void {
  if (log_history_.size() + 1 > max_line_) {
    log_history_.pop_front();
  }
  log_history_.push_back(line);
}

auto kratos::lua::LogHistoryImpl::set_limit(int limit) -> void {
  max_line_ = limit;
}
