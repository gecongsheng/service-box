#include "../detail/os_service.hh"

extern int service_box_main(int argc, const char **argv);

using namespace kratos::service;

int main(int argc, char **argv) {
  OperatingSystemServiceCtrl ctrl;
  auto status = ctrl.check_and_run(argc, argv);
  if (status == OSCtrlStatus::PASS) {
      return service_box_main(argc, (const char**)argv);
  } else if (status == OSCtrlStatus::FAIL) {
      return -1;
  } else {
      return 0;
  }
}
