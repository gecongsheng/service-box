﻿#include "load_balancer/load_balance.h"

namespace kratos {
namespace loadbalance {

auto RRSBalancer::add_lb_node(LoadBalanceNodeWeakPtr lbnode) -> bool {
  if (lbnode.expired()) {
    return false;
  }
  server_count_++;
  nodes_.emplace_back(lbnode);
  return true;
}

auto RRSBalancer::get_next(const std::string &/*ip*/) -> LoadBalanceNodeWeakPtr {
  if (server_count_ == 0) {
    return LoadBalanceNodeWeakPtr();
  }
  last_select_index_ = (last_select_index_ + 1) % server_count_;
  return nodes_[last_select_index_];
}

auto RRSBalancer::clear() -> void {
  last_select_index_ = -1;
  server_count_ = 0;
  nodes_.clear();
  return;
}

} // namespace loadbalance
} // namespace kratos
