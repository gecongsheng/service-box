#include "bool_impl.h"
#include "token.h"

BoolImpl::BoolImpl(bool value) { _value = value; }
BoolImpl::~BoolImpl() {}
bool BoolImpl::get() { return _value; }

AttributePtr BoolImpl::doOperate(int op, AttributePtr rhs) {
  if (op == Token::LOGIC_NOT_EQUAL) {
    return std::make_shared<BoolImpl>(_value != rhs->boolean()->get());
  } else if (op == Token::LOGIC_EQUAL) {
    return std::make_shared<BoolImpl>(_value == rhs->boolean()->get());
  } else if (op == Token::LOGIC_AND) {
    return std::make_shared<BoolImpl>(_value && rhs->boolean()->get());
  } else if (op == Token::LOGIC_OR) {
    return std::make_shared<BoolImpl>(_value || rhs->boolean()->get());
  } else if (op == Token::EXCL_MARK) {
    return std::make_shared<BoolImpl>(!_value);
  }
  return nullptr;
}

auto BoolImpl::to_string() -> std::string {
  return (_value ? "true" : "false");
}
