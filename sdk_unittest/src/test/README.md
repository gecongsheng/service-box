# 编写测试用例

1. 调用`get_proxy`方法可以获取对应服务的代理，直接调用方法即可

```

SDK_CASE(TestServiceMethod1) {
    get_prx()->ServiceMethod1(...);
}

```

2. 调用`get_sdk`方法获取SDK框架

3. 调用`quit_case`方法退出当前用例
```
SDK_CASE(TestServiceMethod1) {
    ....
    quit_case();
    return;
    ...
}
```

4. 调用`get_sdk()->write_log`方法写入日志